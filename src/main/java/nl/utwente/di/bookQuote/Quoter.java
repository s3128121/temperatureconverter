package nl.utwente.di.bookQuote;

import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class Quoter {
    HashMap<String, Double> isbnAndResults = new HashMap<>();
    public Quoter() {
        fillHashMap();
    }

    public double getBookPrice(String isbn) {
        if (Objects.equals(isbn, "1") || Objects.equals(isbn, "2") ||
                Objects.equals(isbn, "3") || Objects.equals(isbn, "4") ||
                Objects.equals(isbn, "5")){
            return isbnAndResults.get(isbn);
        }
        return 0;
    }

    public void fillHashMap(){
        isbnAndResults.put("1", 10.0);
        isbnAndResults.put("2", 45.0);
        isbnAndResults.put("3", 20.0);
        isbnAndResults.put("4", 35.0);
        isbnAndResults.put("5", 50.0);
    }
}
